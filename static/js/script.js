$(function () {
    $('.up').on('click', function () {
        var item = $(this).closest('.accordion-item');
        item.insertBefore(item.prev());
    });
    $('.down').on('click', function () {
        var item = $(this).closest('.accordion-item');
        item.insertAfter(item.next());
    });
});

$(".text").on('click', function(e){
    var panel = $(this).parent().siblings('.content');
    if (e.target.className != "down" && e.target.className != "up"){
        panel.toggle();
    }
})
